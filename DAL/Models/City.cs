﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class City
    {
        public int CityId { get; set; }
        public string Title { get; set; }
        public string PostCode { get; set; }
        public string Kladr { get; set; }

        //public IEnumerable<Department> Departments { get; set; }
    }
}
