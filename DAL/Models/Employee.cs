﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class Employee
    {
        public int EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string BirthDate { get; set; }
        public string Passport { get; set; }
        public string PhoneNumber { get; set; }
        public string Position { get; set; }
        public int Salary { get; set; }

        public int? SecretaryId { get; set; }
        public int? BossId { get; set; }

        public int DepartmentId { get; set; }
        //public Department Department { get; set; }
    }
}
