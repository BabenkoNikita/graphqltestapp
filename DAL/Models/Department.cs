﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class Department
    {
        public int DepartmentId { get; set; }
        public string Title { get; set; }
        public string Address { get; set; }
        public int CityId { get; set; }

        //public City City { get; set; }
        //public IEnumerable<Employee> Employees { get; set; }
    }
}
