﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using RandomNameGenerator;

namespace DAL
{
    internal static class EmployeeData
    {
        public static List<City> Cities { get; set; }
        public static List<Department> Departments { get; set; }
        public static List<Employee> Employees { get; set; }

        static EmployeeData()
        {
            Cities = new List<City>
            {
                new City
                {
                    CityId = 74,
                    Title = "Chelyabinsk",
                    Kladr = "7400000000000",
                    PostCode = "454000"
                },
                new City
                {
                    CityId = 55,
                    Title = "Omsk",
                    Kladr = "5500000000000"
                }
            };

            Departments = new List<Department>
            {
                new Department
                {
                    DepartmentId = 7400,
                    Title = "Department of Chelyabinsk region",
                    Address = "Gagarina 40",
                    CityId = 74
                },
                new Department
                {
                    DepartmentId = 7403,
                    Title = "Department of sales",
                    Address = "Vorovskogo 77",
                    CityId = 74
                },
                new Department
                {
                    DepartmentId = 5500,
                    Title = "Department of Omsk region",
                    Address = "Marksa 33",
                    CityId = 55
                },
                new Department
                {
                    DepartmentId = 5503,
                    Title = "Department of sales",
                    Address = "Lenina 11",
                    CityId = 55
                }
            };

            var r = new Random();

            Employees = new List<Employee>();
            for (int i = 0; i < 400; i++)
            {
                var emp = new Employee();

                emp.EmployeeId = i;

                var genderIsMale = r.Next() % 2 == 0;
                emp.FirstName = NameGenerator.GenerateFirstName(genderIsMale ? Gender.Male : Gender.Female);
                emp.LastName = NameGenerator.GenerateLastName();
                if (r.Next(9) > 2)
                {
                    emp.MiddleName = NameGenerator.GenerateFirstName(Gender.Male);
                }
                emp.BirthDate = DateTime.Now.AddDays(-r.Next(365)).AddYears(-r.Next(20, 60)).Date.ToString("dd.MM.yyyy");

            if (i < 4)
                {
                    emp.DepartmentId = Departments[i].DepartmentId;
                    emp.SecretaryId = i + 4;
                    emp.Position = "Boss";
                } else if (i < 8)
                {
                    emp.DepartmentId = Departments[i - 4].DepartmentId;
                    emp.BossId = i - 4;
                    emp.Position = "Secretary";
                } else
                {
                    var depNumber = r.Next(3);
                    var depId = Departments[depNumber].DepartmentId;
                    emp.DepartmentId = depId;
                    emp.BossId = depNumber;
                    emp.Position = GeneratePosition(r);
                }

                emp.Salary = r.Next(100000);
                emp.Passport = r.Next(9999).ToString().PadRight(4, '0') + " " + r.Next(999999).ToString().PadRight(6, '0');
                emp.PhoneNumber = r.Next(99999).ToString().PadRight(5, '0') + r.Next(99999).ToString().PadRight(5, '0');

                Employees.Add(emp);
            }

        }

        private static string GeneratePosition(Random r)
        {
            var n = r.Next(5);
            switch (n)
            {
                case 0:
                    return "Manager";
                case 1:
                    return "Accountant";
                case 2:
                    return "Engineer";
                case 3:
                    return "HR";
                case 4:
                    return "Security";
                case 5:
                    return "Driver";
                default:
                    return null;
            }
        }
    }
}
