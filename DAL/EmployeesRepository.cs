﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL.Models;

namespace DAL
{
    public class EmployeesRepository : IEmployeesRepository
    {
        public int AddEmployee(Employee employee)
        {
            var id = EmployeeData.Employees.Max(e => e.EmployeeId) + 1;
            employee.EmployeeId = id;
            EmployeeData.Employees.Add(employee);
            return id;
        }

        public int EditEmployee(Employee employee)
        {
            try
            {
                var index = EmployeeData.Employees.FindIndex(e => e.EmployeeId == employee.EmployeeId);
                EmployeeData.Employees[index] = employee;
                return employee.EmployeeId;
            }
            catch (ArgumentNullException ex)
            {
                return -1;
            }
        }

        public Employee GetEmployee(int? id)
        {
            if (!id.HasValue)
            {
                return null;
            }
            return EmployeeData.Employees.FirstOrDefault(e => e.EmployeeId == id);
        }

        public Department GetDepartment(int id)
        {
            return EmployeeData.Departments.FirstOrDefault(d => d.DepartmentId == id);
        }

        public City GetCity(int id)
        {
            return EmployeeData.Cities.FirstOrDefault(c => c.CityId == id);
        }

        public IEnumerable<Department> GetDepartmentsInCity(int cityId)
        {
            return EmployeeData.Departments.Where(d => d.CityId == cityId);
        }

        public IEnumerable<Department> GetDepartments()
        {
            return EmployeeData.Departments;
        }

        public IEnumerable<Employee> GetEmployeesInDepartment(int departmentId)
        {
            return EmployeeData.Employees.Where(e => e.DepartmentId == departmentId);
        }

        public IEnumerable<Employee> GetEmployeesInPosition(int position)
        {
            string pos = position == 1 ? "Boss" : position == 2 ? "Secretary" : null;
            return EmployeeData.Employees.Where(e => e.Position == pos);
        }

        public IEnumerable<City> GetCities()
        {
            return EmployeeData.Cities;
        }
        
    }
}
