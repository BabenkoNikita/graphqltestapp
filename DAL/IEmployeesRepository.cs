﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL
{
    public interface IEmployeesRepository
    {
        Employee GetEmployee(int? id);
        Department GetDepartment(int id);
        City GetCity(int id);
        int AddEmployee(Employee employee);
        int EditEmployee(Employee employee);
        IEnumerable<Department> GetDepartmentsInCity(int cityId);
        IEnumerable<Department> GetDepartments();
        IEnumerable<Employee> GetEmployeesInDepartment(int departmentId);
        IEnumerable<Employee> GetEmployeesInPosition(int position);
        IEnumerable<City> GetCities();

    }
}
