import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
 
import { EmployeeViewComponent }   from './components/employee-view/employee-view.component';
import { CityMenuComponent }   from './components/city-menu/city-menu.component';
import { HomeViewComponent }   from './components/home-view/home-vew.component';
 
const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeViewComponent },
  { path: 'employee/:id', component: EmployeeViewComponent },
  { path: 'mm', component: CityMenuComponent },
];
 
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
