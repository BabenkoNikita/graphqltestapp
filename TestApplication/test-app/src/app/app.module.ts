import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {AccordionModule} from 'primeng/accordion';    
import {MenubarModule} from 'primeng/primeng'; 
import {PanelMenuModule} from 'primeng/panelmenu'; 
import {TableModule} from 'primeng/table';   
import {ButtonModule} from 'primeng/button';
import {InputTextModule} from 'primeng/inputtext';
import {CalendarModule} from 'primeng/calendar';
import {InputSwitchModule} from 'primeng/inputswitch';
import {DropdownModule} from 'primeng/dropdown';
import {SpinnerModule} from 'primeng/spinner';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';

import { HttpClientModule } from '@angular/common/http';
import { ApolloModule, Apollo } from 'apollo-angular';
import { HttpLinkModule, HttpLink } from 'apollo-angular-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';

import { AppComponent } from './app.component';
import { CityMenuComponent } from './components/city-menu/city-menu.component';
import { EmployeeListComponent } from './components/employee-list/employee-list.component';
import { EmployeeViewComponent } from './components/employee-view/employee-view.component';
import { HomeViewComponent } from './components/home-view/home-vew.component';


import { GraphQLService } from './services/graph-ql.service';
import { EmployeeService } from './services/employee.service';
import { AppRoutingModule } from './app-routing.module';
import { GraphQLModule } from './graphql.module';

@NgModule({
  declarations: [
    AppComponent,
    CityMenuComponent,
    EmployeeListComponent,
    EmployeeViewComponent,
    HomeViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    GraphQLModule,
    HttpModule,
    FormsModule, 
    ReactiveFormsModule,
    AccordionModule,
    
    PanelMenuModule,
    TableModule,
    ButtonModule,
    InputTextModule,
    CalendarModule,
    InputSwitchModule,
    DropdownModule,
    SpinnerModule,
    MessagesModule,
    MessageModule,
    MenubarModule,

    BrowserAnimationsModule,
    HttpClientModule,
    ApolloModule,
    HttpLinkModule
  ],
  providers: [
    GraphQLService,
    EmployeeService
  ],
  bootstrap: [AppComponent],
  
})
export class AppModule { }
