import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Employee, Query } from '../../types';
import { GraphQLService } from '../../services/graph-ql.service';

import {Message} from 'primeng/components/common/api';

@Component({
  selector: 'app-employee-view',
  templateUrl: './employee-view.component.html',
  styleUrls: ['../../app.component.css'],
})
export class EmployeeViewComponent implements OnInit {

  msgs: Message[] = [];
  loading: boolean = true;
  editModeOn: boolean = false;
  employee: Employee;
  id: number;
  positions: any[];
  departments: any[] = [];
  employeesInBossPosition: any[] = [];
  employeesInSecretaryPosition: any[] = [];
  countSourseLoaded: number = 0;

  constructor(private graphQLService: GraphQLService, 
    private route: ActivatedRoute,
    private location: Location )
    {
      this.id = +this.route.snapshot.paramMap.get('id');
      this.loadEmlpoyee();
      this.loadEmlpoyeeIPositions();
      this.loadDepartments();
    };

    ngOnInit() {
      this.positions = [
        {label: "Boss", value: "Boss"},
        {label: "Secretary", value: "Secretary"},
        {label: "Accountant", value: "Accountant"},
        {label: "Engineer", value: "Engineer"},
        {label: "HR", value: "HR"},
        {label: "Security", value: "Security"},
        {label: "Driver", value: "Driver"},
        {label: "Accountant", value: "Accountant"}
      ];
    };

    loadEmlpoyee()
    {
        this.graphQLService.getEmployee(this.id)
        .subscribe(({ data }) => {
            this.employee = data.employee;
            this.checkLoading();
        });;
    }

    loadEmlpoyeeIPositions()
    {
      this.graphQLService.getEmployeesInPosition(1)
      .subscribe(({ data }) => {
          data.employeesInPosition.forEach(e => {
            this.employeesInBossPosition.push({label: `${e.firstName} ${e.lastName}`, value: e.employeeId});
          });
          this.checkLoading();
      });
      this.graphQLService.getEmployeesInPosition(2)
        .subscribe(({ data }) => {
            data.employeesInPosition.forEach(e => {
              this.employeesInSecretaryPosition.push({label: `${e.firstName} ${e.lastName}`, value: e.employeeId});
            });
            this.checkLoading();
        });
    }

    loadDepartments()
    {
      this.graphQLService.getDepartments()
      .subscribe(({ data }) => {
          data.departments.forEach(d => {
            this.departments.push({label: d.title, value: d.departmentId});
          });

          this.checkLoading();
      });
    }

    checkLoading()
    {
      this.countSourseLoaded ++;
      if(this.countSourseLoaded == 4 )
      {
        this.loading = false;
      }
    }

    backClicked() {
      this.location.back();
    }

    save() {
      this.msgs = [];

      if(this.employee.salary < 10000 || !this.employee.firstName || !this.employee.lastName || !this.employee.passport)
      {
        this.msgs.push({severity:'error', summary:'Invalid values', detail:'Employee was not saved'});
        return;
      }
      
      //TODO: save
      //this.graphQLService.addEmployee(this.employee);

      this.msgs.push({severity:'success', summary:'Success Message', detail:'Employee successfully saved'});
    }

}
