import { Component, OnInit } from '@angular/core';

import { Employee } from '../../types';
import { GraphQLService } from '../../services/graph-ql.service';
import { EmployeeService } from '../../services/employee.service'

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['../../app.component.css'],
})
export class EmployeeListComponent implements OnInit {

  employees: Employee[];
  cols: any[];
  needToSelectDepartment: boolean = true;

  public currentDepartment: number;

  constructor(private employeeService: EmployeeService, private graphQLService: GraphQLService){
    this.employeeService.listenDepartmentSelect().subscribe((m:number) => {
        this.onDepartmentChange(m);
    })
    };

    ngOnInit() {

        this.cols = [
            { field: 'employeeId', header: 'employeeId' },
            { field: 'firstName', header: 'firstName' },
            { field: 'lastName', header: 'lastName' },
            { field: 'middleName', header: 'middleName' },
            { field: 'birthDate', header: 'birthDate' },
            { field: 'passport', header: 'passport' },
            { field: 'phoneNumber', header: 'phoneNumber' },
            { field: 'position', header: 'position' },
            { field: 'salary', header: 'salary' },
            // { field: 'departmentId', header: 'departmentId' },
            // { field: 'bossId', header: 'bossId' },
            // { field: 'secretaryId', header: 'secretaryId' }
        ];

    };

    onDepartmentChange(event) {
        this.loadEmlpoyees(event);
        this.currentDepartment = event;
        this.needToSelectDepartment = false;
    };

    loadEmlpoyees(departmentId: number)
    {
        this.graphQLService.getEmployees(departmentId)
        .subscribe(({ data }) => {
            this.employees = data.employees;
        });;
    }

}
