import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';

import {MenuItem} from 'primeng/api';

import { City } from '../../types';
import { GraphQLService } from '../../services/graph-ql.service';
import { EmployeeService } from '../../services/employee.service';

@Component({
  selector: 'app-city-menu',
  templateUrl: './city-menu.component.html',
  styleUrls: ['../../app.component.css'],
})
export class CityMenuComponent implements OnInit {

  items: MenuItem[] = [];
  cities: City[];

  loading: boolean;

  constructor(private service: Http, 
    private graphQLService: GraphQLService,
    private employeeService: EmployeeService) {  }

  ngOnInit() {

    this.graphQLService.getCities()
    .subscribe(({ data }) => {
      data.cities.forEach(c =>
        {
          let departmentItems: any[] = [];
          c.departments.forEach(d => departmentItems.push({
            label: d.title, 
            command: () => this.SetSelectedDepartmentId(d.departmentId)
          }));
          this.items.push({
            label: c.title,
            items: departmentItems
          });
        });
    });;
    
  }

  private SetSelectedDepartmentId(n: number)
  {
    this.employeeService.departmentSelect(n);
  }

}
