export type City = {
    cityId: number;
    title: string;
    kladr: string;
    postCode: string;
    departments: Department[];
  }

export type Department = {

  departmentId: number;
  title: string;
  address: string;
  cityId: number;
}

export type Employee = {
  employeeId: number;
  firstName: string;
  lastName: string;
  middleName: string;
  birthDate: string;
  passport: string;
  phoneNumber: string;
  position: string;
  salary: number;

  departmentId: number;
  bossId: number;
  secretaryId: number;
}

export type Query = {
    cities: City[];
    departments: Department[];
    employees: Employee[];
    employeesInPosition: Employee[];
    employee: Employee;
  }