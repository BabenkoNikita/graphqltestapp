import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class EmployeeService {
    private lastDepartmentId: number = 0;
    private _listners = new Subject<number>();

    constructor(){}

    listenDepartmentSelect(): Observable<number> {
       return this._listners.asObservable();
    }

    departmentSelect(departmentId: number) {
        if(this.lastDepartmentId != departmentId)
        {
            this.lastDepartmentId = departmentId;
            this._listners.next(departmentId);
        }
       
    }

}