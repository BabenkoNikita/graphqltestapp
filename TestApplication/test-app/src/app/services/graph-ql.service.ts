import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { Query, Employee } from '../types';
import { Injectable } from '@angular/core';

@Injectable()
  export class GraphQLService {
    constructor(private apollo: Apollo) { }


    public getCities()
    {
      return this.apollo.watchQuery<Query>({
        query: gql`
        query{
          cities{
            cityId
            title
            departments
            {
              departmentId
              title
            }
          }
        }
        `
      }) 
      .valueChanges;
    }

    public getDepartments()
    {
      return this.apollo.watchQuery<Query>({
        query: gql`
        query{
          departments{
            address
            cityId
            departmentId
            title
          }
        }
        `
      }) 
      .valueChanges;
    }

    public getEmployees(departmentId: number)
    {
      return this.apollo.watchQuery<Query>({
        query: gql`
        query{
          employees(departmentid: ${departmentId}){
            birthDate
            bossId
            departmentId
            employeeId
            firstName
            lastName
            middleName
            passport
            phoneNumber
            position
            salary
            secretaryId
          }
        }
        `
      }) 
      .valueChanges;
    }

    public getEmployeesInPosition(position: number)
    {
      return this.apollo.watchQuery<Query>({
        query: gql`
        query{
          employeesInPosition(position: ${position}){
            employeeId
            firstName
            lastName
          }
        }
        `
      }) 
      .valueChanges;
    }

    public getEmployee(employeeId: number)
    {
      return this.apollo.watchQuery<Query>({
        query: gql`
        query{
          employee(employeeid: ${employeeId}){
            birthDate
            bossId
            departmentId
            employeeId
            firstName
            lastName
            middleName
            passport
            phoneNumber
            position
            salary
            secretaryId
          }
        }
        `
      }) 
      .valueChanges;
    }


    public addEmployee(employee: Employee)
    {
      this.apollo.mutate<Query>({
        mutation: gql`
        mutation{
          createEmployee{
            birthDate
            bossId
            departmentId
            employeeId 
            firstName
            lastName
            middleName
            passport
            phoneNumber
            position
            salary
            secretaryId
          }
        }
        `
      }) 
      .subscribe(({ data }) => {
        console.log('got data', data);
      }, (error) => {
        console.log('there was an error sending the query', error);
      });
    }
    

  }