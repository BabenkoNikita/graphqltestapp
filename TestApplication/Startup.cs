﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using GraphiQl;
using GraphQL;
using GraphQL.Types;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TestApplication.GraphQL;
using TestApplication.GraphQL.Types;

namespace TestApplication
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddMvc();

            services.AddTransient<IEmployeesRepository, EmployeesRepository>();
            services.AddSingleton<IDocumentExecuter, DocumentExecuter>();

            services.AddSingleton<EmployeeQuery>();
            services.AddSingleton<EmployeeMutation>();

            services.AddSingleton<CityType>();
            services.AddSingleton<DepartmentType>();
            services.AddSingleton<EmployeeType>();
            services.AddSingleton<EmployeeInputType>();

            var sp = services.BuildServiceProvider();
            services.AddSingleton<ISchema>(new EmployeeSchema(new FuncDependencyResolver(type => sp.GetService(type))));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //}





            DefaultFilesOptions options = new DefaultFilesOptions();
            options.DefaultFileNames.Clear();
            options.DefaultFileNames.Add("index.html");

            app.Use(async (context, next) =>
            {
                await next();

                if (context.Response.StatusCode == 404 && !Path.HasExtension(context.Request.Path.Value))
                {
                    context.Request.Path = "/index.html";
                    await next();
                }
            })
            .UseCors("AllowAll")
            .UseMvc()
            .UseDefaultFiles(options)
            .UseStaticFiles();



            //app.UseDefaultFiles(new DefaultFilesOptions { DefaultFileNames = new List<string> { "index.html" } });


            //app.UseDefaultFiles();
            //app.UseStaticFiles();

            // app.UseCors(builder => builder.WithOrigins("http://localhost:4200"));

            //app.UseGraphiQl();
            //app.UseMvc(routes =>
            //{
            //    routes.MapRoute("API", 
            //        "api/{controller}/{action}/{id?}");
            //    routes.MapRoute("Default",
            //        "{*catchall}",
            //        new { controller = "Home", action = "Index" });
            //    //routes.MapSpaFallbackRoute("spa-fallback", new { controller = "Home", action = "Index" });
            //});

            //app.Use(async (context, next) =>
            //{
            //    await next();

            //    if (context.Response.StatusCode == 404 && !Path.HasExtension(context.Request.Path.Value) && !context.Request.Path.Value.StartsWith("api"))
            //    {
            //        context.Request.Path = "/Index.html";
            //        context.Response.StatusCode = 200;
            //        await next();
            //    }
            //}
            //);
        }
    }
}
