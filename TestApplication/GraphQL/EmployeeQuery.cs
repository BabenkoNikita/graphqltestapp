﻿using DAL;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApplication.GraphQL.Types;

namespace TestApplication.GraphQL
{
    public class EmployeeQuery : ObjectGraphType
    {
        private readonly IEmployeesRepository repo;

        public EmployeeQuery(IEmployeesRepository repo)
        {
            this.repo = repo;

            Field<ListGraphType<CityType>>("cities",
                              resolve: context =>
                              {
                                  return repo.GetCities();
                              });

            Field<ListGraphType<DepartmentType>>("departments",
                              resolve: context =>
                              {
                                  return repo.GetDepartments();
                              });

            Field<ListGraphType<EmployeeType>>("employees",
                            arguments: new QueryArguments(
                              new QueryArgument<IntGraphType>() { Name = "departmentid" }),
                              resolve: context =>
                              {
                                  var id = context.GetArgument<int>("departmentid");
                                  return repo.GetEmployeesInDepartment(id);
                              });

            Field<ListGraphType<EmployeeType>>("employeesInPosition",
                            arguments: new QueryArguments(
                              new QueryArgument<IntGraphType>() { Name = "position" }),
                              resolve: context =>
                              {
                                  var position = context.GetArgument<int>("position");
                                  return repo.GetEmployeesInPosition(position);
                              });

            Field<EmployeeType>("employee",
                            arguments: new QueryArguments(
                              new QueryArgument<IntGraphType>() { Name = "employeeid" }),
                              resolve: context =>
                              {
                                  var id = context.GetArgument<int>("employeeid");
                                  return repo.GetEmployee(id);
                              });

        }
    }

}
