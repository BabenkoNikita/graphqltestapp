﻿using DAL;
using DAL.Models;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApplication.GraphQL.Types;

namespace TestApplication.GraphQL
{
    public class EmployeeMutation : ObjectGraphType
    {
        private readonly IEmployeesRepository repo;

        public EmployeeMutation(IEmployeesRepository repo)
        {
            this.repo = repo;

            Name = "CreateEmployeeMutation";

            Field<EmployeeInputType>(
                "createEmployee",
                resolve: context =>
                {
                    var employee = context.GetArgument<Employee>("employee");
                    return repo.AddEmployee(employee);
                });
        }
    }
}
