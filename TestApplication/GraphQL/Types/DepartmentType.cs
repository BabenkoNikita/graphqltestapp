﻿using DAL;
using DAL.Models;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestApplication.GraphQL.Types
{
    public class DepartmentType: ObjectGraphType<Department>
    {
        public DepartmentType()
        {
            EmployeesRepository repository = new EmployeesRepository();

            Name = "Department";
            Field(f => f.DepartmentId);
            Field(f => f.Title);
            Field(f => f.Address);
            

            Field(f => f.CityId);
            //Field<CityType>(
            //    "City",
            //    resolve: context => repository.GetCity(context.Source.CityId)
            //);

            //Field<ListGraphType<EmployeeType>>(
            //    "Employees",
            //    resolve: context => repository.GetEmployeesInDepartment(context.Source.DepartmentId)
            //);
        }
    }
}
