﻿using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestApplication.GraphQL.Types
{
    public class EmployeeInputType : InputObjectGraphType
    {
        public EmployeeInputType()
        {
            Name = "EmployeeInput";

            Field<IntGraphType>("employeeId");
            Field<NonNullGraphType<StringGraphType>>("firstName");
            Field<NonNullGraphType<StringGraphType>>("lastName");
            Field<StringGraphType>("middleName");

            Field<DateGraphType>("birthDate");
            Field<NonNullGraphType<StringGraphType>>("passport");
            Field<StringGraphType>("phoneNumber");
            Field<StringGraphType>("position");
            Field<IntGraphType>("salary");

            Field<IntGraphType>("departmentId");
            Field<NonNullGraphType<IntGraphType>>("bossId");
            Field<NonNullGraphType<IntGraphType>>("secretaryId");
        }
    }
}
