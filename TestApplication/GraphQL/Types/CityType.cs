﻿using DAL;
using DAL.Models;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestApplication.GraphQL.Types
{
    public class CityType: ObjectGraphType<City>
    {
        public CityType()
        {
            EmployeesRepository repository = new EmployeesRepository();

            Name = "City";
            Field(f => f.CityId);
            Field(f => f.Title);
            Field(f => f.Kladr, nullable: true);
            Field(f => f.PostCode, nullable: true);

            Field<ListGraphType<DepartmentType>>(
                "Departments",
                resolve: context => repository.GetDepartmentsInCity(context.Source.CityId)
            );
        }
    }
}
