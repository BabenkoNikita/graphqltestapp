﻿using DAL;
using DAL.Models;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestApplication.GraphQL.Types
{
    public class EmployeeType: ObjectGraphType<Employee>
    {
        public EmployeeType()
        {
            EmployeesRepository repository = new EmployeesRepository();

            Name = "Employee";
            Field(f => f.EmployeeId);
            Field(f => f.FirstName);
            Field(f => f.LastName);
            Field(f => f.MiddleName, nullable: true);

            Field(f => f.BirthDate);
            Field(f => f.Passport);
            Field(f => f.PhoneNumber, nullable: true);
            Field(f => f.Position);
            Field(f => f.Salary);

            Field(f => f.DepartmentId);
            //Field<DepartmentType>(
            //    "Department", 
            //    resolve: context => repository.GetDepartment(context.Source.DepartmentId)
            //);

            Field(f => f.BossId, nullable: true);
            //Field<EmployeeType>(
            //    "Boss",
            //    resolve: context => repository.GetEmployee(context.Source.BossId)
            //);

            Field(f => f.SecretaryId, nullable: true);
            //Field<EmployeeType>(
            //    "Secretary",
            //    resolve: context => repository.GetEmployee(context.Source.SecretaryId)
            //);
        }
    }
}
