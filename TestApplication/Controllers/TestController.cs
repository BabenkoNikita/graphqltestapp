﻿using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestApplication.Controllers
{
    [Route("/api/[Controller]")]
    public class TestController : Controller
    {
        private IEmployeesRepository repo { get; set; }

        public TestController()
        {
            repo = new EmployeesRepository();
        }

        [HttpGet("Test")]
        public IEnumerable<string> Test()
        {
            return new List<string> { "one", "Two" };
        }

        [HttpGet("TestEmployees")]
        public IEnumerable<Employee> GetTestEmployees()
        {
            return repo.GetEmployeesInDepartment(7400);
        }

    }
}
